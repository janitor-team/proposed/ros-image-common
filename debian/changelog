ros-image-common (1.12.0-12) unstable; urgency=medium

  * Drop nose dependency (Closes: #1018627)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 29 Aug 2022 14:44:21 +0200

ros-image-common (1.12.0-11) unstable; urgency=medium

  * Add patch for new pluginlib

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 08 Jul 2022 12:41:21 +0200

ros-image-common (1.12.0-10) unstable; urgency=medium

  * Add patch for new pluginlib
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 13 Jun 2022 13:02:05 +0200

ros-image-common (1.12.0-9) unstable; urgency=medium

  * Fix test logic

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 27 Oct 2021 00:20:41 +0200

ros-image-common (1.12.0-8) unstable; urgency=medium

  * Ignore failing tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 26 Oct 2021 23:49:19 +0200

ros-image-common (1.12.0-7) unstable; urgency=medium

  * Enable tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 26 Oct 2021 22:41:11 +0200

ros-image-common (1.12.0-6) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:42:32 +0200

ros-image-common (1.12.0-5) experimental; urgency=medium

  * Team upload.
  * Do not install ROS package manifest twice (Closes: #994878)

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 15:38:35 +0200

ros-image-common (1.12.0-4) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Mon, 20 Sep 2021 18:54:20 +0200

ros-image-common (1.12.0-3) unstable; urgency=medium

  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 16:02:26 +0100

ros-image-common (1.12.0-2) unstable; urgency=medium

  * Add patch for Boost 1.74
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 14 Dec 2020 14:11:38 +0100

ros-image-common (1.12.0-1) unstable; urgency=medium

  * New upstream version 1.12.0
  * Remove Thomas from Uploaders, thanks for working on this
  * rebase patches
  * bump debhelper version

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 19 Jun 2020 12:24:18 +0200

ros-image-common (1.11.13-8) unstable; urgency=medium

  * Fix boost python version string (Closes: #962287)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 05 Jun 2020 18:48:45 +0200

ros-image-common (1.11.13-7) unstable; urgency=medium

  * Add missing build dependency
  * Support multiple Python version
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 06 Apr 2020 16:32:36 +0200

ros-image-common (1.11.13-6) unstable; urgency=medium

  * Add patch for cmake python module
  * Drop ROS_PYTHON_VERSION from d/rules (not needed)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 23 Dec 2019 11:07:13 +0100

ros-image-common (1.11.13-5) unstable; urgency=medium

  * Drop Python 2 packages (Closes: #938381)
  * Bump policy version (no changes)
  * simplify d/watch

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 26 Oct 2019 06:55:48 +0200

ros-image-common (1.11.13-4) unstable; urgency=medium

  * Limit d/watch to ROS1
  * switch to debhelper-compat and debhelper 12
  * Bump policy version (no changes)
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 23 Aug 2019 12:55:07 +0200

ros-image-common (1.11.13-3) unstable; urgency=medium

  * Depend on libyaml-cpp-dev instead of libyaml0.3-cpp-dev which is now
    obsolete (Closes: #918161).

 -- Simon Quigley <tsimonq2@debian.org>  Thu, 03 Jan 2019 16:43:41 -0600

ros-image-common (1.11.13-2) unstable; urgency=medium

  * Add Python 3 package
  * Update copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 03 Nov 2018 16:23:10 +0100

ros-image-common (1.11.13-1) unstable; urgency=medium

  * Fix watch file
  * New upstream version 1.11.13
  * Rebase patch
  * Fix description-contains-invalid-control-statement
  * Bump policy version (no changes)
  * cleanup rules

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 29 Oct 2018 22:24:48 +0100

ros-image-common (1.11.11-3) unstable; urgency=medium

  * add Multi-Arch according to hinter
  * Update policy and debhelper versions
  * Remove -pie from hardening.
    Thanks to Adrian Bunk (Closes: #865739)
  * Update watch file
  * Remove parallel (Default for debhelper 10)
  * Update Vcs URLs to salsa.d.o
  * Add R³
  * http -> https
  * Update policy and debhelper versions
  * Add missing dependency (Closes: #896398)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 21 Apr 2018 09:47:36 +0200

ros-image-common (1.11.11-2) unstable; urgency=medium

  * Move package.xml to lib for plugin export line

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 03 Dec 2016 10:41:38 +0100

ros-image-common (1.11.11-1) unstable; urgency=medium

  * New upstream version 1.11.11

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 28 Sep 2016 09:43:49 +0200

ros-image-common (1.11.10-6) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Update my email address

  [ Daniele E. Domenichelli ]
  * Add missing build dependencies (Closes: #835689)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 30 Aug 2016 22:51:40 +0200

ros-image-common (1.11.10-5) unstable; urgency=medium

  * Team upload.
  * debian/rules: disable -pie from the hardening buildflags.
    Fix FTBFS in several architectures.  Closes: #818715
    This was caused by a latent bug triggerend by my cleanup in the last upload.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 20 Mar 2016 16:16:08 +0000

ros-image-common (1.11.10-4) unstable; urgency=medium

  * Team upload.
  * debian/rules: remove useless included file
  * Add appropriate Breaks/Replaces after moving files in the previous between
    binaries in the previous uploads.  Closes: #817813

 -- Mattia Rizzolo <mattia@debian.org>  Fri, 18 Mar 2016 20:13:31 +0000

ros-image-common (1.11.10-3) unstable; urgency=medium

  * Added camera-calibration-parsers-tools
  * Moved binaries to camera-calibration-parsers-tools (Closes: #815874)
  * Some lintian warnings
  * Updated Vcs-Browser and Vcs-Git fields
  * Moved usr/lib/polled_camera/poller to polled-camera-tool package
  * Added hardening options
  * Bump Standards-Version to 3.9.7 (no changes)

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Tue, 01 Mar 2016 10:38:20 +0100

ros-image-common (1.11.10-2) unstable; urgency=medium

  * Move libimage_transport_plugins.so to libimage-transport0d
  * Adopt to new libexec location in catkin (Closes: #815218)

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 21 Feb 2016 12:41:07 +0100

ros-image-common (1.11.10-1) unstable; urgency=medium

  * Add python-camera-calibration-parsers
  * Imported Upstream version 1.11.10
  * Update patches

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Tue, 19 Jan 2016 23:06:33 +0100

ros-image-common (1.11.8-2) unstable; urgency=medium

  * Add dependencies

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 10 Jan 2016 15:10:08 +0100

ros-image-common (1.11.8-1) unstable; urgency=medium

  * Initial release (Closes: #805331)

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sat, 26 Dec 2015 00:29:10 +0000
